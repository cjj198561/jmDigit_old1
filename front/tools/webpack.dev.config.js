var conf = require('../server/config')
var getWebPackConfig = require('./webpack.config.js')

module.exports = getWebPackConfig({
  env: 'development',
  dev_hot_server_host: conf.dev_hot_server_host,
  dev_hot_server_port: conf.dev_hot_server_port
})
