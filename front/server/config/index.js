const config = {
  platform: 5, // platform:平台 1:芭菲婚礼 2:金色百年 3:CMS 4:捷拍 5:简码
  env: process.env.NODE_ENV || 'development',
  listen_port: (process.env.NODE_ENV === 'production') ? '80' : '9000',
  dev_hot_server_host: 'localhost',
  dev_hot_server_port: '9001',
  rethink_db_host: (process.env.NODE_ENV === 'production') ? 'rethinkdb' : 'rethinkdb',
  rethink_db_port: (process.env.NODE_ENV === 'production') ? '28015' : '28015',
  api_host: (process.env.NODE_ENV === 'production') ? 'gohost' : 'gohost',
  api_port: (process.env.NODE_ENV === 'production') ? '8000' : '8000'
}

module.exports = config
