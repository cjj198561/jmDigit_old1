import ajaxHandle from '../../common/js/ajax/ajax.js'
import config from '../../common/js/config/config.js'
import { list, detail } from './data.js'

class ServiceOrders {
  constructor() {
    this.isDev = config.runMode === 'dev' ? true : false
    this.hostUrl = config.apiHost
  }

  list(param, callback) {
    ajaxHandle.query({
      url: `${this.hostUrl}/api/customer/orderList`,
      param: param,
      callback: this.isDev ? (res) => {
        callback(list)
      } : callback
    })
  }

  details(param, callback) {
    ajaxHandle.post({
      url: `${this.hostUrl}/api/customer/orderDetail`,
      param: param,

      callback: this.isDev ? (res) => {
        callback(detail)
      } : callback
    });
  }
}
const serviceOrders = new ServiceOrders()

export default serviceOrders
