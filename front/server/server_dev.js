import koa from "koa";

const loadMiddleware = (path, app) => {
  require(path).default(app)
}
const loadRouter = (path, app) => {
  let router = require(path).default
  app.use(router.routes(), router.allowedMethods())
}

const app = new koa()

loadMiddleware('./middleware/webpack', app)
loadRouter('./middleware/dev_static_file', app)
loadRouter('./routers/public_proxy', app)
loadMiddleware('./middleware/static_file', app)
loadMiddleware('./middleware/favicon', app)
loadMiddleware('./middleware/bodyparser', app)
loadMiddleware('./middleware/ejs', app)
loadMiddleware('./middleware/session', app)
loadMiddleware('./middleware/attach', app)

loadRouter('./routers/base', app)
loadRouter('./routers/home', app)
loadMiddleware('./middleware/auth', app)

var config = require('./config/index')
app.listen(config.listen_port)
console.log(config.env, config.listen_port)
