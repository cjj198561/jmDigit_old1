/**
 * Created by jinsebainian on 2017/7/29.
 */

import React from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import { AuthStatus } from '../../utils/code_def'

// 未认证的情况下显示去认证界面
const UnauthAlert = (props) =>{
  const { authStatus, authLink  } = props
  switch (authStatus){
    case AuthStatus.UnAuth:{
      return (
        <div className="u-unauth-alert">
          尊敬客户您好，你当前状态是<strong>未认证</strong>，<br/>
          请到<Link to={authLink}>基本信息</Link>页面完成认证，<br/>
          系统审核通过以后请重新登录系统。<br/>
        </div>
      )
    }
    case AuthStatus.Wait:{
      return (
        <div className="u-unauth-alert">
          尊敬客户您好，你当前状态是<strong>认证中</strong>，<br/>
          审核人员会在3个工作日内,审核完成，<br/>
          请耐心等待，系统审核通过以后请重新登录系统<br/>
        </div>
      )
    }
    case AuthStatus.NoPass:{
      return (
        <div className="u-unauth-alert">
          尊敬客户您好，你当前状态是<strong>认证失败</strong>，<br/>
          请到<Link to={authLink}>基本信息</Link>页面重新完成认证，<br/>
          系统审核通过以后请重新登录系统。<br/>
        </div>
      )
    }
  }
}
export default UnauthAlert