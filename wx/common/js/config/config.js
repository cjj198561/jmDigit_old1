const config = {
  // 模式：prod dev test
  runMode: 'dev',
  // api主机域名
  apiHost: 'https://sp.jsbn.com',
}

export default config
