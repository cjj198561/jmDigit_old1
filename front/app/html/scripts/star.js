/*
  R 半径基础
  M 移动距离
  D 最远距离
  N 生成小圆的数量
 */
const [R,
    M,
    D,
    N] = [14, 2, 150, 30];
class Circle {
    /**
     * [构造函数]
     * @param  {[int]} x [x轴坐标]
     * @param  {[int]} y [x轴坐标]
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.r = Math.random() * R + 1; //
        this._mx = Math.random() * M - 1;
        this._my = Math.random() * M - 1;
    }
    /**
     * [drawCircle 画小圆]
     * @param  {[canvas]} ctx [canvas 对象]
     */
    drawCircle(ctx) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0, 360);
        ctx.closePath();
        ctx.fillStyle = 'rgba(204, 204, 204, 0.2)';
        ctx.fill();
    }
    /**
     * [drawLine 两圆之间画一条线]
     * @param  {[obj]} ctx     [canvas 对象]
     * @param  {[obj]} _circle [圆对象]
     */
    drawLine(ctx, _circle) {
        let dx = this.x - _circle.x;
        let dy = this.y - _circle.y;
        let d = Math.sqrt(dx * dx + dy * dy);
        if (d < D) {
            ctx.beginPath();
            ctx.moveTo(this.x, this.y); // 起始点
            ctx.lineTo(_circle.x, _circle.y); // 终点
            ctx.closePath();
            ctx.strokeStyle = 'rgba(204, 204, 204, 0.1)';
            // ctx.strokeStyle = 'rgba(20,20,20,0.1)';
            ctx.stroke();
        }
    }
    
    /**
     * [move 移动坐标]
     * @param  {[int]} w [画布宽]
     * @param  {[int]} h [画布高]
     */
    move(w, h) {
        this._mx = (this.x < w && this.x > 0)
            ? this._mx
            : (-this._mx);
        this._my = (this.y < h && this.y > 0)
            ? this._my
            : (-this._my);
        this.x += this._mx / 2;
        this.y += this._my / 2;
    }
}
// 当前鼠标所对应的小圆
class currentCircle extends Circle {
    constructor(x, y) {
        super(x, y);
    }
    drawCircle(ctx) {
        ctx.beginPath();
        // this.r = (this.r < R && this.r > 1)
        //     ? this.r + (Math.random() * M - 1)
        //     : M;
        this.r = M * 3;
        ctx.arc(this.x, this.y, this.r, 0, 360);
        ctx.closePath();
        ctx.fillStyle = 'rgba(100, 100, 100, ' + (parseInt(Math.random() * 100) / 100) + ')';
        ctx.fill();
    }
}
window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

let canvas = document.querySelector("#top");
let ctx = canvas.getContext("2d");

let w = canvas.width = canvas.offsetWidth;
let h = canvas.height = canvas.offsetHeight;

let circles = [];
let current_circle = new currentCircle(0, 0);

let draw = function() {
    ctx.clearRect(0, 0, w, h);
    for (let i = 0; i < circles.length; i++) {
        circles[i].move(w, h);
        circles[i].drawCircle(ctx);
        for (let j = i + 1; j < circles.length; j++) {
            circles[i].drawLine(ctx, circles[j])
        }
    }
    if (current_circle.x) {
        // current_circle.drawCircle(ctx);
        for (let k = 1; k < circles.length; k++) {
            current_circle.drawLine(ctx, circles[k]);
        }
    }
    requestAnimationFrame(draw);
}

let init = function(num) {
    for (let i = 0; i < num; i++) {
        circles.push(new Circle(Math.random() * w, Math.random() * h));
    }
    draw();
}

window.addEventListener('load', init(N));
window.onmousemove = function(e) {
    e = e || window.event;
    current_circle.x = e.clientX;
    current_circle.y = e.clientY;
},
window.onmouseout = function() {
    current_circle.x = null;
    current_circle.y = null;
};
