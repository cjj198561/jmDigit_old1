/*
 * Created by cjj on 2017/7/31
 * body解析中间件
*/
import bodyparser from 'koa-bodyparser'

export default (app) => {
  app.use(bodyparser())
}
