/**
 * Created by chenjianjun on 16/8/10.
 */
import cache from "./cache";

class CacheManger {
  constructor() {
    this.myCache = cache.createCache();
  }

  static Instance() {
    if (this.instance == null) {
      // 创建单例
      this.instance = new CacheManger();
    }

    return this.instance;
  }

  get(key) {
    return this.myCache.get(key)
  }

  set(key, value, timeOut) {
    this.myCache.set(key, value, timeOut)
  }

  clear() {
    this.myCache.clear();
  }

  clearWithKey(key) {
    this.myCache.clearWithKey(key);
  }
}

export default CacheManger