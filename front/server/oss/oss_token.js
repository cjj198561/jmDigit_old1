/**
 * Created by chenjianjun on 16/8/13.
 */
import ALY from "aliyun-sdk";
import CacheManger from "../cache/cache_manger";

class OSSTokenApi {
  static Instance() {
    if (this.instance == null) {
      // 创建单例
      this.instance = new OSSTokenApi()
    }
    return this.instance
  }

  constructor() {
    // 生成用户, 生成成功的时候回自动生成一个文件,里面就有accessKeyId,secretAccessKey  roleARN是在角色管理里面查看
    // 通过sts去获取角色信息
    this.sts = new ALY.STS({
      accessKeyId: 'LTAIdRTvcQfp9VGq',
      secretAccessKey: 'CBqaA881wYVBziEOV0mmURANsoAIeA',
      endpoint: 'https://sts.aliyuncs.com',
      apiVersion: '2015-04-01'
    })
  }

  getToken() {
    return new Promise((resolve, reject) => {
      let token = CacheManger.Instance().get('ossToken')
      if (token !== null) {
        let res = {
          success: true,
          message: '',
          count: 1,
        }
        res.data = [{...token.Credentials}]
        resolve(res);
      } else {
        this.sts.assumeRole({
          // 指定角色的资源描述符；每个角色都有一个唯一的资源描述符(Arn)，您可以在RAM的角色管理中查看一个角色的Arn
          RoleArn: 'acs:ram::1712472832694546:role/wh-oss-role',
          // 此参数用来区分不同的Token，以标明谁在使用此Token，便于审计;
          RoleSessionName: 'wh-oss-role',
          // 过期时间,秒
          DurationSeconds: 3600
        }, (err, data) => {
          let res = {
            success: true,
            message: '',
            count: 1,
          }
          if (err) {
            res.success = false
            res.data = [{}]
          } else {
            // 过期时间(秒)
            let expiration = parseInt(((new Date(data.Credentials.Expiration)).getTime() - (new Date()).getTime()) / 1000)
            CacheManger.Instance().set('ossToken', data, (expiration - 180)*1000) // 设置缓存超时为返回的过期时间与三分钟的差距
            res.data = [{...data.Credentials}]
          }
          resolve(res);
        })
      }
    })
  }
}

export default OSSTokenApi
