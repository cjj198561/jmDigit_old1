/*
 * Created by cjj on 2017/7/31
 * webpack中间件
 */
import webpack from 'webpack'
import webpackDevServer from 'webpack-dev-server'
import config from '../config'
import webpackConfig from '../../tools/webpack.dev.config.js'

export default (app) => {
  let bundleStart = null;

  const compiler = webpack(webpackConfig);
  compiler.plugin('compile', () => {
    console.log('Bundling...');
    bundleStart = Date.now();
  });

  compiler.plugin('done', () => {
    console.log(`Bundled in ${Date.now() - bundleStart} ms!`);
  });

  const bundler = new webpackDevServer(compiler, {
    publicPath: webpackConfig.output.publicPath,
    historyApiFallback: true,
    hot: true,
    quiet: false,
    noInfo: true,
    stats: {
      colors: true
    }
  });

  bundler.listen(config.dev_hot_server_port, 'localhost', () => {
    console.log('Bundling project, please wait...');
  })
}
