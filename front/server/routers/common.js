import React from "react";
import shortId from "shortid";
import _ from "lodash";
import { renderToString } from "react-dom/server";

import config from "../config";

const porcHandle = (p, routeNodes, isServerRender = true, getCusParamsFunc = null) => {
  return async (ctx, next) => {
    let params = {
      // 用户的参数
      ...p.params,
      // 开发模式
      mode: config.env,
      // 填充服务器获取到的一些客户端信息,比如是否是移动端访问
      ...ctx.clientInfo,
      // 渲染类型(server client)
      renderType: 'server',
      // 用于客户端生成文件传输key,或者作为其他唯一key使用
      keyId: shortId.generate(),
      // session信息
      ...ctx.session.user,
    }

    if (getCusParamsFunc) {
      params = _.merge(params, await getCusParamsFunc(ctx, next))
    }

    let context = {}
    let markup = ''
    let c = routeNodes(params, p.initState, ctx.url, context)

    if (isServerRender) {
      markup = renderToString(
        c.render
      )
    }
    if (context.url) {
      ctx.redirect(context.url)
    } else {
      await ctx.render('tpl', {
        // 版本号
        version: '1.00',
        // 标题
        title: p.title || '',
        // 渲染html字符串
        markup: markup,
        // js名称
        templateName: p.templateName,
        // 初始状态传递到客户端渲染
        initState: encodeURIComponent(JSON.stringify(c.store.getState())),
      })
    }
  }
}

export default porcHandle
