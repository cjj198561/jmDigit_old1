import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Button, Card, Icon, Input, Modal, Table, Tag } from "antd";
import BaseComponent from "./base.jsx";
import { OperateType, TabColumnType } from "../../utils/code_def";
import ImageItem from "./image_item.jsx";

const FooterCondition = ({ ...props }) => {
  const { siftFilter } = props
  return (
    <div>
      <strong style={{ paddingRight: 10 }}>搜索条件:</strong>
      {
        _.map(siftFilter, (v, k) => {
          return (
            <Tag key={k}>{v.value}</Tag>)
        })
      }
    </div>
  )
}
FooterCondition.propTypes = {
  siftFilter: PropTypes.object.isRequired,
}

class TableList extends BaseComponent('TableList') {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const paginationDefault = {
      showTotal: (total, range) => {
        return (`${range[ 0 ]}-${range[ 1 ]} , 共 ${total} 条记录`)
      }
    }
    const { dataSource, pagination, loading, onTableFilterChange } = this.props

    return (
      <Table columns={this.getColumns()}
             dataSource={dataSource}
             pagination={_.merge(pagination, paginationDefault)}
             onChange={onTableFilterChange}
             loading={loading}
             bordered
             title={this.getTitle}
             footer={this.getFooter}/>
    )
  }

  getTitle = () => {
    const { onOperateAction, titleName } = this.props

    return (
      <div className="table-title-box">
        <div className="title-box">
          <icon type=""/>
          <span className="text">{titleName}</span>
        </div>
        {
          onOperateAction &&
          <Button.Group>
            <Button icon="sync" onClick={() => onOperateAction(OperateType.REFRESH)}>刷新</Button>
            <Button type="primary" icon="plus" onClick={() => onOperateAction(OperateType.ADD)}>创建</Button>
          </Button.Group>
        }
      </div>
    )
  }

  getFooter = () => {
    const { siftFilter = {} } = this.props
    return <FooterCondition siftFilter={siftFilter}/>
  }

  getColumns = () => {
    const { columns, onTableFilterChange, pagination } = this.props
    return _.map(columns, (v, k) => {
      let n = {
        ...v
      }

      switch (v.type) {
        case TabColumnType.Search: {
          if (this.state[ v.key ] === undefined) {
            this.state[ v.key ] = {
              filterDropdownVisible: false
            }
          }
          n.filterDropdown = (
            <div className={v.attributes.className}>
              <Input ref={node => this[ v.key ] = node} placeholder={v.attributes.placeholder}/>
              <Button type={v.attributes.searchButtonType} onClick={(e) => {
                onTableFilterChange(pagination, { [`${v.attributes.searchKey}`]: [ `${this[ v.key ].refs.input.value}` ] }, {})
                this.setState({ [v.key]: { filterDropdownVisible: false } })
              }}>{v.attributes.searchName}</Button>
            </div>
          )
          n.filterIcon = (<Icon type={v.attributes.filterIconName}/>)
          n.filterDropdownVisible = this.state[ v.key ].filterDropdownVisible
          n.onFilterDropdownVisibleChange = (visible) => {
            this.setState({ [v.key]: { filterDropdownVisible: visible } }, () => this[ v.key ].focus())
          }

          // 是否有特殊渲染
          if (v.attributes.render) {
            n.render = v.attributes.render
          }

          break
        }
        case TabColumnType.DropDown: {
          // 是否可以多选
          n.filterMultiple = v.attributes.filterMultiple
          // 获取下拉值列表
          n.filters = v.attributes.filters()
          // 获取筛选的默认值
          n.filteredValue = v.attributes.filtered()
          // 是否有特殊渲染
          if (v.attributes.render) {
            n.render = v.attributes.render
          }

          break
        }
        case TabColumnType.Picture: {
          //数据为图片时,增加图片列Column
          n.className = n.className ? n.className + " img-column " : "img-column"
          n.render = (text, record, index) => {
            return (
              <ImageItem
                imageUrl={text}
                height={40}
                aspectRatio={v.attributes.aspectRatio}
                quality={80}
                processType={v.attributes.processType}
                water={false}/>
            )
          }
          n.onCellClick = (record, e) => {
            Modal.info({
              iconType: null,
              width: 'auto',
              className: "model-large-img",
              maskClosable: true,
              content: (
                <div style={{ textAlign: 'center', fontSize: 0 }}>
                  <Card style={{ display: 'inline-block' }} bodyStyle={{ padding: 0 }}>
                    <ImageItem
                      imageUrl={record[ v.key ]}
                      width={600}
                      aspectRatio={v.attributes.aspectRatio}
                      quality={100}
                      processType={v.attributes.processType}
                      water={v.attributes.water}/>
                  </Card>
                </div>
              ),
            })
          }
          // 是否有特殊渲染
          if (v.attributes.render) {
            n.render = v.attributes.render
          }
        }
      }

      return n
    })
  }
}

TableList.propTypes = {
  dataSource: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  siftFilter: PropTypes.object.isRequired,
  titleName: PropTypes.string.isRequired,
  columns: PropTypes.array.isRequired,
  onTableFilterChange: PropTypes.func.isRequired,
  onOperateAction: PropTypes.func,
}

export default TableList
