import netApi, { Method } from "../../../utils/net";
import util from "../../../utils/util";

// 请求数据
export const queryData = (dispatch, type, method, url, params, hintMsg) => {
  let callback = (err, json) => {
    if (err) {
      dispatch(
        fail(type, { errMsg: `网络异常... ${err}`, queryParams: params })
      )
    } else {
      if (json.success) {
        dispatch(
          result(type, { ...json, queryParams: params })
        )
      } else {
        dispatch(
          fail(type, { errMsg: json.message, queryParams: params })
        )
      }
    }
  }

  dispatch(loading(type, { hintMsg, queryParams: params }))

  if (method === Method.GET) {
    netApi.get(util.buildQueryUrl(url, params), callback)
  } else if (method === Method.POST) {
    netApi.post(url, params, callback)
  }
}

// 请求中
export const loading = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 1,
      ...payload
    }
  }
}
// 请求结果
export const result = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 2,
      ...payload
    }
  }
}
// 请求失败
export const fail = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 3,
      ...payload,
    }
  }
}
