import staticFile from 'koa-static'

export default (app) => {
  app.use(staticFile(__dirname + '/../../public', {'maxage':60*60*1000}))
}
