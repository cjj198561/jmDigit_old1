import { getDateWithDay } from "../../common/js/utils/date.js"
import serviceOrders from '../../service/orders/orders'
import { list, detail } from '../../service/orders/data.js'
var app = getApp();
Page({
  data: {
    customList: [],
    offsetDays: 0,
    dateTitle: getDateWithDay(0),
    errMsg: '',
  },
  onLoad:function(){
    this.setData({
      customList: list.data,
      offsetDays: 0,
      dateTitle: getDateWithDay(0),
      errMsg: '',
    });
  },
  /**
  * 用户点击右上角分享
  */
  onShareAppMessage: function () {
    let title = '捷拍-抓住每一个精彩瞬间'
    return {
      title: title,
      path: 'pages/sp/list/list'
    }
  },

  onPullDownRefresh: function () {
    this.fetchList(this.data.offsetDays);
  },
  tapForward: function () {
    let offsetDays = this.data.offsetDays - 1
    this.setData({
      customList: list.data,
      offsetDays: offsetDays,
      dateTitle: getDateWithDay(offsetDays),
      errMsg:"",
    });
  },

  tapNext: function () {
    let offsetDays = this.data.offsetDays + 1
    this.setData({
      customList: list.data,
      offsetDays: offsetDays,
      dateTitle: getDateWithDay(offsetDays),
      errMsg: "",
    });
  },
  fetchList: function (offsetDays) {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })

    serviceOrders.list({ offsetDays: offsetDays }, (res) => {
      wx.hideLoading()
      if (res.success) {
        let list = []
        for (let i = 1; i < (res.data || []).length; i++) {
          let v = Object.assign({}, res.data[i])
          v.customerHeadImg = dealwithPath.dealWith_GD_S_S(v.customerHeadImg, 200, 200)
          list.push(v)
        }

        // 调用成功
        this.setData({
          customList: list,
          offsetDays: offsetDays,
          dateTitle: getDateWithDay(offsetDays),
          errMsg: '',
        });
      } else {
        // 调用失败，显示错误信息
        this.setData({
          customList: [],
          offsetDays: offsetDays,
          dateTitle: getDateWithDay(offsetDays),
          errMsg: res.message,
        });
      }
    });
  },
  // 跳转到code界面
  goCode: function (e) {
    // 获取当前客户的id
    let id = e.currentTarget.dataset.id;
    let url = '../detail/detail?id=' + id;
    wx.navigateTo({
      url: url,
    })
  },
});
