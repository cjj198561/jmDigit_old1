/*
 * Created by cjj on 2017/7/31
 * 模板中间件
 */
import path from 'path'
import ejsEngine from 'koa-ejs'

export default (app) => {
  ejsEngine(app, {
    root: path.join(__dirname, '/../../views'),
    layout: 'tpl',
    viewExt: 'ejs',
    cache: true,
    debug: false
  })
}
