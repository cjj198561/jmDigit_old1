import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Upload } from "antd";
import OSSClient from "../../utils/oss_client";

class FileUpload extends React.Component {
  constructor(props) {
    super(props)
    const { renderType, mode, keyId } = this.props
    this.oss = new OSSClient(renderType, mode, keyId)
    this.state = {
      fileList: this.dealWith(props.value)
    }
  }

  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      this.setState({ fileList: this.dealWith(nextProps.value) })
    }
  }

  dealWith(videoUrl) {
    if (videoUrl && videoUrl.length > 0) {
      return [ {
        uid: -1,
        name: videoUrl,
        status: 'done',
        url: videoUrl,
      } ]
    }

    return []
  }

  handleChange = (info) => {
    let fileList = info.fileList
    if (info.file.status === 'done') {
      this.setState({ fileList: this.dealWith(info.file.response) })
      this.triggerChange(info.file.response)
    } else {
      this.setState({ fileList: fileList })
    }
  }

  handleRemove = (file) => {
    if (file.url) {
      this.oss.deleteFile(file.url)
    }
    this.setState({ fileList: [] })
    this.triggerChange('')
  }

  triggerChange = (videoUrl) => {
    const onChange = this.props.onChange
    if (onChange) {
      onChange(videoUrl)
    }
  }

  handUpload = (fs) => {
    const { isFileNameRound = false, uploadDir, suffix = '' } = this.props
    fs.onProgress({ percent: 20 }, fs.file)

    if (isFileNameRound) {
      this.oss.upWithFileName(fs.file, this.props.uploadDir).then((fp) => {
        let p = fs.onSuccess(fp, fs.file)
      }).catch(err => {
        fs.onError(err, null, fs.file)
      })
    } else {
      this.oss.uploadFile(fs.file, this.props.uploadDir, suffix).then((fp) => {
        let p = fs.onSuccess(fp, fs.file)
      }).catch(err => {
        fs.onError(err, null, fs.file)
      })
    }
  }

  render() {
    const { readOnly = false, accept, multiple = false } = this.props
    const { fileList = [] } = this.state

    let uP = {
      type: 'file',
      multiple: multiple,
      accept: accept,
      supportServerRender: true,
      customRequest: this.handUpload,
      fileList: fileList
    }

    if (readOnly) {
      uP.disabled = true
    } else {
      uP.onRemove = this.handleRemove
      uP.onChange = this.handleChange
    }

    return (
      <Upload {...uP}>
        {
          fileList.length > 0
            ?
            null
            :
            <Button>
              <Icon type="upload"/> upload
            </Button>
        }
      </Upload>
    )
  }
}

FileUpload.propTypes = {
  renderType: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  isFileNameRound: PropTypes.bool,
  accept: PropTypes.string.isRequired,
  suffix: PropTypes.string
}

export default FileUpload
