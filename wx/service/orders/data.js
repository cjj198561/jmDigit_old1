const list = {
  "success": true,
  "count": 0,
  "message": "",
  "data": [
    {
      "id": 5,
      "serviceTime": "2017-06-20",
      "serviceAddress": "二郎2号厅",
      "customerName": "钟欣/任贞玲",
      "customerHeadImg": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082005244&di=27ba278775a29a3485fb66497047ac83&imgtype=0&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F12%2F84%2F06%2F27G58PICw8W.jpg"
    },
    {
      "id": 4,
      "serviceTime": "2017-06-20",
      "serviceAddress": "解放碑1号厅",
      "customerName": "李波/石红",
      "customerHeadImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504676720&di=3babbb2ba8b6b11e3b2aa9e31d92c1c1&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F15%2F15%2F12%2F19I58PICBNu_1024.jpg"
    },
    {
      "id": 3,
      "serviceTime": "2017-06-20",
      "serviceAddress": "九滨5号厅",
      "customerName": "苟嘉 陈媛媛",
      "customerHeadImg": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082005244&di=388efc7b526a2b0b6b3aa0d05afdab80&imgtype=0&src=http%3A%2F%2Fpic6.wed114.cn%2F20150311%2F2015031115480555327717.jpg"
    },
    {
      "id": 2,
      "serviceTime": "2017-06-20",
      "serviceAddress": "九滨8号厅",
      "customerName": "宝宝",
      "customerHeadImg": "https://www.wed114.cn/baike/uploads/allimg/160310/27-160310161153.jpg"
    },
    {
      "id": 1,
      "serviceTime": "2017-06-20",
      "serviceAddress": "九滨芭菲婚礼会馆3号厅",
      "customerName": "测试用户",
      "customerHeadImg": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082498688&di=632b417b2ef7cee73c7e1291cb494b08&imgtype=0&src=http%3A%2F%2Fimg05.tooopen.com%2Fimages%2F20150511%2Ftooopen_sy_123802063513.jpg"
    }
  ]
}

const detail = { 
  "success": true, 
  "count": 0, 
  "message": "", 
  "data": { 
    "customerName": "钟欣/任贞玲", 
    "photoDetails": [{ 
      "id": 40, 
      "procUrl": "https://www.wed114.cn/baike/uploads/allimg/160310/27-160310161153.jpg", 
      "width": 2048, 
      "height": 1152 
      }, 
      { 
        "id": 50, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082005244&di=388efc7b526a2b0b6b3aa0d05afdab80&imgtype=0&src=http%3A%2F%2Fpic6.wed114.cn%2F20150311%2F2015031115480555327717.jpg", 
        "width": 2048, 
        "height": 1245 
      }, 
      { 
        "id": 51, 
        "procUrl": "https://www.wed114.cn/baike/uploads/allimg/160310/27-160310161153.jpg", 
        "width": 2048, 
        "height": 1226 
      }, 
      { 
        "id": 52, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082498688&di=632b417b2ef7cee73c7e1291cb494b08&imgtype=0&src=http%3A%2F%2Fimg05.tooopen.com%2Fimages%2F20150511%2Ftooopen_sy_123802063513.jpg", 
        "width": 2048, 
        "height": 1365 
      }, 
      { 
        "id": 53, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504676720&di=3babbb2ba8b6b11e3b2aa9e31d92c1c1&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F15%2F15%2F12%2F19I58PICBNu_1024.jpg", 
        "width": 2048, 
        "height": 1365 
      }, 
      { 
        "id": 54, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082005244&di=388efc7b526a2b0b6b3aa0d05afdab80&imgtype=0&src=http%3A%2F%2Fpic6.wed114.cn%2F20150311%2F2015031115480555327717.jpg", 
        "width": 2048, 
        "height": 1152 
      }, 
      { 
        "id": 56, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504676720&di=3babbb2ba8b6b11e3b2aa9e31d92c1c1&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F15%2F15%2F12%2F19I58PICBNu_1024.jpg", 
        "width": 2048, 
        "height": 1262 
      }, 
      { 
        "id": 57, 
        "procUrl": "https://www.wed114.cn/baike/uploads/allimg/160310/27-160310161153.jpg", 
        "width": 2048, 
        "height": 1096 
      }, 
      { 
        "id": 58, 
        "procUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1504082498688&di=632b417b2ef7cee73c7e1291cb494b08&imgtype=0&src=http%3A%2F%2Fimg05.tooopen.com%2Fimages%2F20150511%2Ftooopen_sy_123802063513.jpg", 
        "width": 2048, 
        "height": 1365 
      }] 
  } 
}

export {
  list,
  detail
}
