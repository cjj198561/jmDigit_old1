var Promise = require('../../../plugins/es6-promise.js')

/*
  对函数做封装，使之支持Promise调用
*/
function PromiseHandle(fn) {
  return (obj = {}) => {
    return new Promise((resolve, reject) => {
      obj.success = (res) => {
        resolve(res)
      }

      obj.fail = (res) => {
        reject(res)
      }

      fn(obj)
    })
  }
}

export default PromiseHandle
