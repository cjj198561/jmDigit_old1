module.exports = {
  formatTime: function (date) {
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let hour = date.getHours()
    let minute = date.getMinutes()
    let second = date.getSeconds()

    return `${year}/${month}/${day} ${hour}:${minute}:${second}`
  },
  formatDate: function (date) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    return `${year}-${month}-${day}`
  },
  getAllDate: function () {
    let date = new Date()
    let dates = {
      now: '',
      years: [],
      months: [],
      days: [],
      value: [9999, 1, 1]
    }

    dates.now = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();

    for (let i = 2017; i <= date.getFullYear(); i++) {
      dates.years.push(i)
    }

    for (let i = 1; i <= 12; i++) {
      dates.months.push(i)
    }

    for (let i = 1; i <= 31; i++) {
      dates.days.push(i)
    }

    return dates
  },
  getDateWithDay: function (day) {
    let date = new Date()
    date.setDate(date.getDate() + day)
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
  }
}
