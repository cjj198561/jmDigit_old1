#! /bin/sh

cd $GOPATH/src/jm

# go get -u -v github.com/go-sql-driver/mysql
# go get -u -v github.com/bitly/go-simplejson
# go get -u -v github.com/beego/bee

case "$1" in
  run)
  	bee run
	;;
  doc)
  	bee generate docs
	;;
  runwithdoc)
    bee run -gendoc=true -downdoc=true
	;;
  docwithrun)
  	bee run -downdoc=true -gendoc=true
	;;
  pack)
    bee pack
  ;;
  *)
	echo $"Usage       : $0 {run|doc|runwithdoc|docwithrun|pack}"
  echo $"run         : 运行程序"
  echo $"doc         : 编译文档"
  echo $"runwithdoc  : 带swagger运行"
  echo $"docwithrun  : 编译文档后带swagger运行"
  echo $"pack        : 打包应用"
	exit 2
esac

exit $?
