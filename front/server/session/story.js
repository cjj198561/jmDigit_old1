/**
 * Created by chenjianjun on 16/3/23.
 * koa-generic-session 的自定义插件
 */
var _ = require('lodash')
var config = require('../config/index')
var debug = require('debug')('session:generic-session-rethinkdb')
var Thinky = require('thinky')({
  db: 'sessions',
  host: config.rethink_db_host,
  port: config.rethink_db_port
});

var type = Thinky.type;
var r = Thinky.r

const Sessions = Thinky.createModel('sessions', {
  sid: type.string(),
  updateTime: type.date().default(r.now()),
  cookie: {
    expires: type.date().default(r.now()),
    httpOnly: type.boolean(),
    maxage: type.number(),
    overwrite: type.boolean(),
    path: type.string(),
    signed: type.boolean(),
  },
  user: {},
})
Sessions.ensureIndex('sid');

function ThinkySession() {
  this.model = Sessions
}

ThinkySession.prototype.get = function*(sid) {
  debug('get', sid)
  var res = yield this.model.filter({ sid: sid }).run()
  debug('got', res[ 0 ])
  return res[ 0 ]
}

ThinkySession.prototype.set = function*(sid, session) {
  // 根据sid查询session是否存在
  debug('set', sid, session)
  var res = yield this.model.filter({ sid: sid }).run()
  if (res[ 0 ]) {
    // 更新
    return yield this.model.filter({ sid: sid }).update({ cookie: session.cookie, updateTime: session.cookie.expires })
  } else {
    // 新的session需要删除同一用户的老session
    debug('new session', session)
    if (session.user && session.user.userName) {
      yield this.model.filter({
        user: {
          userName: session.user.userName,
          userType: session.user.userType
        }
      }).delete()
    }

    var dt = new Date()
    dt.setMilliseconds(dt.getMilliseconds() + session.cookie.maxage || session.cookie.maxAge);
    // 插入
    let payload = _.extend({
      sid: sid,
      updateTime: dt
    }, session)

    return yield this.model.save(payload)
  }
}

ThinkySession.prototype.destroy = function*(sid) {
  debug('destroy', sid)
  return yield this.model.filter({ sid: sid }).delete();
}

module.exports = ThinkySession
