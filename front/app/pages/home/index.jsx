/*
 * @file 首页的入口文件
 */
import React from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import routeNodes from "./router.jsx";
import Compatibility from "../../common/compatibility";

// 处理兼容性
Compatibility()

// 传递给客户端的初始状态值
let initStateString = document.getElementById('J_Matrix').attributes[ 'data-init-state' ].nodeValue || '{}'
let initState = JSON.parse(decodeURIComponent(initStateString))
let params = initState.params

params.renderType = 'client'

let ctx = routeNodes(params, _.omit(initState, 'params'))
ReactDOM.render(
  ctx.render,
  document.getElementById('J_Main')
)
