/*
* @object
* url: 服务器接口地址
* param: 请求的参数
* callback: 回调函数
*/
import PromiseHandle from '../promise/promise.js'

class Ajax {
  query(object) {
    PromiseHandle(wx.request)({
      url: object.url,
      data: object.param,
      method: 'GET',
      dataType: 'json'
    }).then(res => {
      this.responseWrapper(res, object.callback)
    }).catch(res => {
      this.responseWrapper(res, object.callback)
    })
  }

  post(object) {
    PromiseHandle(wx.request)({
      url: object.url,
      data: object.param,
      method: 'POST',
      dataType: 'json'
    }).then(res => {
      this.responseWrapper(res, object.callback)
    }).catch(res => {
      this.responseWrapper(res, object.callback)
    })
  }

  responseWrapper(res, callback) {
    if (!res || res.statusCode != 200) {
      callback({
        success: false,
        count: 0,
        message: res.errMsg || '网络错误',
        data: null
      });
      return;
    }

    if (typeof callback == 'function') {
      callback(res.data);
    }
  }
}

const ajaxHandle = new Ajax()

export default ajaxHandle
