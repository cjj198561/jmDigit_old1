import update from "react-addons-update";
import netApi from "../../utils/net";
import util from "../../utils/util";
import { OperateType } from "../../utils/code_def";

const QUERY_LIST = 'QUERY_LIST'
const QUERY_DETAILS = 'QUERY_DETAILS'
const POST_DATA = 'POST_DATA'
const CLEAR_LIST = 'CLEAR_LIST'
const CLEAR_DETAILS = 'CLEAR_DETAILS'
const LIST_ERR_CONFIRM = 'LIST_ERR_CONFIRM'
const DETAILS_ERR_CONFIRM = 'DETAILS_ERR_CONFIRM'

// ------------------------------------
// Actions
// ------------------------------------
// 请求中
const loading = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 1,
      ...payload
    }
  }
}
// 请求成功
const result = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 2,
      ...payload
    }
  }
}
// 请求失败
const fail = (type, payload) => {
  return {
    type: type,
    payload: {
      LoadType: 3,
      ...payload
    }
  }
}

const listErrConfirm = (prefix) => {
  return {
    type: `${prefix}${LIST_ERR_CONFIRM}`,
  }
}

const detailsErrConfirm = (prefix) => {
  return {
    type: `${prefix}${DETAILS_ERR_CONFIRM}`,
  }
}

const clearList = (prefix) => {
  return {
    type: `${prefix}${CLEAR_LIST}`,
  }
}

const clearDetails = (prefix) => {
  return {
    type: `${prefix}${CLEAR_DETAILS}`,
  }
}

const getListData = (dispatch,
                     getState,
                     reducer, // 模块减速器名
                     prefix,// 前缀,用于拼装action type
                     keyId,// keyId,用于表示数据中一行的唯一key
                     url,// api路径
                     operateType,// 操作类型
                     queryParams // 请求参数
) => {

  // 拼装action type
  let type = `${prefix}${QUERY_LIST}`

  dispatch(loading(type, { hintMsg: '加载中...', operateType }))
  netApi.get(url, (err, json) => {
    if (err) {
      dispatch(fail(type, { errMsg: '网络异常...', operateType }))
    } else {
      if (json.success) {
        dispatch(result(type, { ...json, queryParams, keyId, operateType }))
      } else {
        dispatch(fail(type, { errMsg: json.message, operateType }))
      }
    }
  })
}

const getDetailsData = (dispatch,
                        getState,
                        reducer, // 模块减速器名
                        prefix,// 前缀,用于拼装action type
                        keyId,// keyId,用于表示数据中一行的唯一key
                        url,// api路径
                        operateType,// 操作类型
                        queryParams // 请求参数
) => {

  // 拼装action type
  let type = `${prefix}${QUERY_DETAILS}`

  dispatch(loading(type, { hintMsg: '加载中...', operateType }))
  netApi.get(url, (err, json) => {
    if (err) {
      dispatch(fail(type, { errMsg: '网络异常...', operateType }))
    } else {
      if (json.success) {
        dispatch(result(type, { ...json, queryParams, keyId, operateType }))
      } else {
        dispatch(fail(type, { errMsg: json.message, operateType }))
      }
    }
  })
}

const postData = (dispatch,
                  getState,
                  reducer, // 模块减速器名
                  prefix,// 前缀,用于拼装action type
                  keyId,// keyId,用于表示数据中一行的唯一key
                  url,// api路径
                  listUrl,// 数据列表路径,处理成功以后需要刷新列表
                  operateType,// 操作类型
                  body // body
) => {

  // 拼装action type
  let type = `${prefix}${POST_DATA}`

  dispatch(loading(type, { hintMsg: '处理中...', operateType }))

  netApi.post(url, body, (err, json) => {
    if (err) {
      dispatch(fail(type, { errMsg: '网络异常...', operateType }))
    } else {
      if (json.success) {
        dispatch(result(type, { ...json, operateType }))
        if (operateType === OperateType.DELETE) {
          let params = { ...getState()[ reducer ].list.queryParams }
          let qUrl = util.buildQueryUrl(listUrl, params)
          getListData(dispatch, getState, reducer, prefix, keyId, qUrl, operateType, params)
        }
      } else {
        dispatch(fail(type, { errMsg: json.message, operateType }))
      }
    }
  })
}
const fetchListRefresh = (reducer, prefix, keyId, url, operateType, p = {}) => {
  return (dispatch, getState) => {
    if (getState()[ reducer ].list.fetchFlg) {
      return
    }

    let params = { ...getState()[ reducer ].list.queryParams, ...p }
    let qUrl = util.buildQueryUrl(url, params)
    getListData(dispatch, getState, reducer, prefix, keyId, qUrl, operateType, params)
  }
}
const fetchList = (reducer, prefix, keyId, url, operateType, params = {}) => {
  return (dispatch, getState) => {
    if (getState()[ reducer ].list.fetchFlg) {
      return
    }

    let newParams = { ...getState()[ reducer ].list.queryParams, ...params }
    // 比较条件有没有变化
    if (JSON.stringify(newParams) === JSON.stringify(getState()[ reducer ].list.queryParams)) {
      return
    }

    // 排除分页有没有变化
    if (JSON.stringify(_.omit(newParams, 'pageIndex')) !== JSON.stringify(_.omit(getState()[ reducer ].list.queryParams, 'pageIndex'))) {
      newParams.pageIndex = 1
    }

    let qUrl = util.buildQueryUrl(url, newParams)
    getListData(dispatch, getState, reducer, prefix, keyId, qUrl, operateType, newParams)
  }
}
const fetchDetails = (reducer, prefix, keyId, url, operateType, params = {}) => {
  return (dispatch, getState) => {
    if (getState()[ reducer ].details.fetchFlg) {
      return
    }

    let qUrl = util.buildQueryUrl(url, params)
    getDetailsData(dispatch, getState, reducer, prefix, keyId, qUrl, operateType, params)
  }
}
const fetchPost = (reducer, prefix, keyId, url, listUrl, operateType, body) => {
  return (dispatch, getState) => {
    if (getState()[ reducer ].details.fetchFlg) {
      return
    }

    postData(dispatch, getState, reducer, prefix, keyId, url, listUrl, operateType, body)
  }
}

const comActions = {
  loading,
  result,
  fail,
  listErrConfirm,
  detailsErrConfirm,
  fetchList,
  fetchListRefresh,
  fetchDetails,
  fetchPost,
  clearList,
  clearDetails,
}

const comActionHandle = {
  [QUERY_LIST]: (state, action) => {
    let st = { ...state }

    switch (action.payload.LoadType) {
      case 1: // 请求中
      {
        st.list = update(st.list, {
          fetchFlg: { $set: true },
          errMsg: { $set: '' },
        })

        break
      }
      case 2: // 请求成功
      {
        const { keyId, data = [], count = 0, queryParams = {} } = action.payload
        st.list = update(st.list, {
          fetchFlg: { $set: false },
          errMsg: { $set: '' },
          data: {
            $set: _.map(data, (v, k) => {
              return {
                ...v,
                key: v[ keyId ]
              }
            })
          },
          total: { $set: count },
          queryParams: { $set: { ...queryParams } }
        })

        break
      }
      case 3:// 请求失败
      {
        st.list = update(st.list, {
          fetchFlg: { $set: false },
          errMsg: { $set: action.payload.errMsg },
        })

        break
      }
    }

    return st
  },
  [QUERY_DETAILS]: (state, action) => {
    let st = { ...state }

    switch (action.payload.LoadType) {
      case 1: // 请求中
      {
        st.details = update(st.details, {
          fetchFlg: { $set: true },
          hintMsg: { $set: action.payload.hintMsg },
          errMsg: { $set: '' },
        })

        break
      }
      case 2: // 请求成功
      {
        st.details = update(st.details, {
          fetchFlg: { $set: false },
          errMsg: { $set: '' },
          hintMsg: { $set: '' },
          data: { $set: action.payload.data[0] }
        })

        break
      }
      case 3:// 请求失败
      {
        st.details = update(st.details, {
          fetchFlg: { $set: false },
          hintMsg: { $set: '' },
          errMsg: { $set: action.payload.errMsg },
        })

        break
      }
    }

    // 表明是加载操作
    st.details.postFlg = false

    return st
  },
  [CLEAR_LIST]: (state, action) => {
    let st = { ...state }

    st.list = update(st.list, {
      $set: { ...comInitState.list },
    })

    return st
  },
  [CLEAR_DETAILS]: (state, action) => {
    let st = { ...state }

    st.details = update(st.details, {
      $set: { ...comInitState.details },
    })

    return st
  },
  [POST_DATA]: (state, action) => {
    let st = { ...state }

    switch (action.payload.LoadType) {
      case 1: // 请求中
      {
        if (action.payload.operateType === OperateType.DELETE) {
          st.list = update(st.list, {
            fetchFlg: { $set: true },
            errMsg: { $set: '' }
          })
        } else {
          st.details = update(st.details, {
            fetchFlg: { $set: true },
            errMsg: { $set: '' }
          })
        }

        break
      }
      case 2: // 请求成功
      {
        if (action.payload.operateType === OperateType.DELETE) {
          st.list = update(st.list, {
            fetchFlg: { $set: false },
            errMsg: { $set: '' }
          })
        } else {
          st.details = update(st.details, {
            fetchFlg: { $set: false },
            errMsg: { $set: '' }
          })
        }

        break
      }
      case 3:// 请求失败
      {
        if (action.payload.operateType === OperateType.DELETE) {
          st.list = update(st.list, {
            fetchFlg: { $set: false },
            errMsg: { $set: action.payload.errMsg }
          })
        } else {
          st.details = update(st.details, {
            fetchFlg: { $set: false },
            errMsg: { $set: action.payload.errMsg }
          })
        }

        break
      }
    }

    if (action.payload.operateType !== OperateType.DELETE) {
      // 表明是提交操作
      st.details.postFlg = true
    }

    return st
  },
  [LIST_ERR_CONFIRM]: (state, action) => {
    let st = { ...state }

    st.list = update(st.list, {
      errMsg: { $set: '' }
    })

    return st
  },
  [DETAILS_ERR_CONFIRM]: (state, action) => {
    let st = { ...state }

    st.details = update(st.details, {
      errMsg: { $set: '' },
      // 设置提交状态未初始状态
      postFlg: { $set: false }
    })

    return st
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
const comInitState = {
  list: {
    fetchFlg: false,
    errMsg: '',
    data: [],
    total: 0,
    queryParams: {
      pageIndex: 1,
      pageSize: 10,
    },
  },
  details: {
    fetchFlg: false,
    postFlg: false,// 提交状态
    hintMsg: '',
    errMsg: '',
    data: {},
  },
  cond: {}
}

export {
  comActions,
  comActionHandle,
  comInitState,
}
