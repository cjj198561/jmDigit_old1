$(document).ready(function () {
  document.body.addEventListener('touchstart', function () { /*...空函数即可*/
  });

  insertNavHeader();
  insertFootet();

  //限制微信安卓版网页字体放大
  wxSize();

  ///导航交互js
  u_nav_header();
});

//限制微信安卓版网页字体放大
function wxSize() {
  if (typeof WeixinJSBridge === "object" && typeof WeixinJSBridge.invoke === "function") {
    handleFontSize();
  } else {
    if (document.addEventListener) {
      document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
    } else if (document.attachEvent) {
      document.attachEvent("WeixinJSBridgeReady", handleFontSize);
      document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
    }
  }
  function handleFontSize() {
    // 设置网页字体为默认大小
    WeixinJSBridge.invoke('setFontSizeCallback', { 'fontSize': 0 });
    // 重写设置网页字体大小的事件
    WeixinJSBridge.on('menu:setfont', function () {
      WeixinJSBridge.invoke('setFontSizeCallback', { 'fontSize': 0 });
    });
  }
}

///导航相关js
function u_nav_header() {
  //在平板 和小屏幕分辨率下 点击打开右侧菜单
  $("#menu-switch").bind('click', function () {
    $("#root").toggleClass("open-menu");
    $(".u-nav-header").toggleClass("show-right-menu");
  })

  //获取导航高度
  var topNavHeader = 0;
  if ($(".u-nav-header").length > 0) {
    topNavHeader = $(".u-nav-header")[ 0 ].clientHeight;
  }
  //滚动时根据滚动距离设置起导航背景色
  $(window).scroll(function (e) {
    var scrollTop = $(window).scrollTop();
    var color = "#000";
    if (scrollTop < topNavHeader) {
      color = "rgba(0,0,0," + (scrollTop !== 0 ? parseFloat(scrollTop) / topNavHeader : 0) + ")"
    }
    $(".u-nav-header").css("background-color", color);
  });
}

function insertNavHeader() {

  var $mainNav = $("#root>.u-nav-header")[ 0 ];
  var menuList = [
    {
      url: "'home" + ".html'",
      title: "首页",
      icon: "icon-home",
    },
    {
      url: "'case" + ".html'",
      title: "案例",
      icon: "icon-case",
    },
    {
      url: "'about-us" + ".html'",
      title: "关于我们",
      icon: "icon-about-us",
    },
    {
      url: "'cooperation" + ".html'",
      title: "发布需求",
      icon: "icon-cooperation",
    },
    {
      url: "'join-us" + ".html'",
      title: "加入我们",
      icon: "icon-join-us",
    },
  ];

  var liArray = "";
  for (var i in menuList) {

    liArray +=
      " <li>  " +
      "   <a href= " + menuList[ i ].url + "> " +
      "     <i class='iconfont " + menuList[ i ].icon + "'></i>  " +
      "     <span class='title'>" + menuList[ i ].title + "</span> " +
      "   </a>  " +
      " </li> ";
  }
  var insertMainNav =
    "<div class='header-main'>" +
    " <div id='menu-switch' class='menu-link'>" +
    "   <i class='iconfont icon-switch-menu'></i>" +
    " </div>" +
    " <div class='logo-wrap'>" +
    "   <a href='/'><img src='./images/logo.png' alt='科技有限公司'></a>" +
    " </div>" +
    " <div class='nav-wrap'>" +
    "  <nav>" +
    "   <ul>" + liArray + "</ul>" +
    "  </nav>" +
    " </div>" +
    "</div> ";

  $(insertMainNav).prependTo($mainNav);
  var pathNmae = window.location.pathname;
  var fileName = pathNmae.substring(pathNmae.lastIndexOf('/') + 1, pathNmae.length);
  $($mainNav).find("li>a[href^='" + fileName + "']").parent().addClass("active");
}
function insertFootet() {
  if ($(".u-footer").length > 0) {
    var $insertFootet = $(".u-footer")[ 0 ];
    var footetHtml =
      "<div class='footer-box'>" +
      "Copyright ©重庆网络科技有限公司 2017 All Rights Reserved<br>沪ICP备 13042954号-4" +
      "</div>"
    $(footetHtml).prependTo($insertFootet);
  }
}
