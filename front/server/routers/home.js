import KoaRouter from "koa-router";
import procHandle from "./common";

// 首页
const homeRouter = new KoaRouter()
homeRouter.get(
  '/',
  procHandle(
    {
      // 标题
      title: '简码数字',
      // 客户端渲染依赖的js文件名
      templateName: 'home',
    },
    require('../../app/pages/home/router.jsx').default,
  )
)

export default homeRouter
