/*
 * Created by cjj on 2017/7/31
 * session存储中间件
 */
import session from 'koa-generic-session'
import convert from 'koa-convert'

import connection from '../session/story'
var sessionStore = new connection()

export default (app) => {
  app.keys = ['ty_cms']
  app.use(convert(session({
    store: sessionStore,
    cookie: {
      httpOnly: true,
      path: '/',
      overwrite: true,
      signed: true,
      maxAge: 4 * 60 * 60 * 1000 //毫秒 4小时
    }
  })))
}
