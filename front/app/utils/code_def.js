// 列表列类型
const TabColumnType = {
  Normal: 1, // 正常
  Search: 2, // 搜索
  DropDown: 3, // 下拉
  Picture: 4, // 图片
}

// 操作类型
const OperateType = {
  LIST: 0, // 列表
  DETAILS: 1, // 详情
  ADD: 2, // 新增
  DELETE: 3, // 删除
  UPDATE: 4, // 修改
  REFRESH: 5, // 刷新
  AUTH: 6, // 审核
  ALLOT: 7, //分配
}

// 微信用户关注状态
const WxSubscribeState = {
  None: -1,
  Not: 0,
  Is: 1,
}

export {
  TabColumnType,
  OperateType,
  WxSubscribeState,
}
