/*
 * Created by cjj on 2017/7/31
 * 认证中间件，认证用户是否登录
 * 1.未登录，跳转到登录界面
 * 2.已登录，更新session过期时间
 */
import { UserType } from "../../app/utils/code_def";

// 更新session失效时间
const updateExpires = async (ctx, next) => {
  // 每次操作激活一下cookie的过期时间 TODO:延长时间,目前是一天
  let dt = new Date()
  dt.setMinutes(dt.getMinutes() + 60 * 24)
  ctx.session.cookie.expires = dt

  await next()
}

export default (app) => {
  app.use(async (ctx, next) => {
    // 获取用户的用户类型
    let userType = -1
    if (ctx.session.sid) {
      userType = ctx.session.user.userType
    }
    // 根据用户类型判断访问的url是否合法
    if (ctx.url.startsWith('/api/cameraman') || ctx.url.startsWith('/cameraman')) {
      if (userType === UserType.CAMERAMAN) {
        return await updateExpires(ctx, next)
      }
    } else if (ctx.url.startsWith('/api/merchant') || ctx.url.startsWith('/merchant')) {
      if (userType === UserType.MERCHANT) {
        return await updateExpires(ctx, next)
      }
    } else if (ctx.url.startsWith('/api/system') || ctx.url.startsWith('/system')) {
      if (userType === UserType.SYSTEM) {
        return await updateExpires(ctx, next)
      }
    } else if (ctx.url.startsWith('/api/retoucher') || ctx.url.startsWith('/retoucher')) {
      if (userType === UserType.RETOUCHER) {
        return await updateExpires(ctx, next)
      }
    } else if (ctx.url.startsWith('/api/qe') || ctx.url.startsWith('/qe')) {
      if (userType === UserType.QE) {
        return await updateExpires(ctx, next)
      }
    } else {
      // URL不合法，重定向到大首页
      return ctx.redirect('/')
    }

    // 访问不合法，清空session
    ctx.session = null
    if (ctx.url.startsWith('/api')) {
      ctx.body = {
        success: false,
        message: "",
        data: [ '/login' ],
        code: 302,
        count: 0
      }
    } else {
      ctx.redirect('/login')
    }
  })
}
