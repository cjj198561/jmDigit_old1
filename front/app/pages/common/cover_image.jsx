import React from "react";
import PropTypes from "prop-types";
import { Icon, Upload } from "antd";
import BaseComponent from "./base.jsx";
import ImageItem, { EmImgProcessType } from "./image_item.jsx";
import OSSClient from "../../utils/oss_client";

class CoverImage extends BaseComponent('CoverImage') {
  constructor(props) {
    super(props)

    const { renderType, mode, keyId } = props
    this.oss = new OSSClient(renderType, mode, keyId)

    this.state = {
      previewVisible: false,
      previewImage: '',
      coverUrl: props.value || '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      this.setState({ coverUrl: this.dealWith(nextProps.value) })
    }
  }

  dealWith(coverUrl) {
    if (coverUrl && coverUrl.length > 0) {
      return coverUrl
    }

    return this.state.coverUrl
  }

  handleCancel = () => {
    this.setState({
      previewVisible: false
    })
  }

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  handleChange = (info) => {
    if (info.file.status === 'done') {
      this.triggerChange(info.file.response)
    }
  }

  handleRemove = (file) => {
    if (file.response) {
      this.oss.deleteFile(file.response)
    }
    this.triggerChange('')
  }

  triggerChange = (coverUrl) => {
    const { onChange } = this.props
    if (onChange) {
      onChange(coverUrl)
    }

    if (!('value' in this.props)) {
      this.setState({ coverUrl })
    }
  }

  handUpload = (fs) => {
    fs.onProgress({ percent: 20 }, fs.file)

    this.oss.uploadFile(fs.file, this.props.uploadDir).then((fp) => {
      fs.onSuccess(fp, fs.file)
    }).catch(err => {
      fs.onError(err, null, fs.file)
    })
  }

  render() {
    const { coverUrl } = this.state
    const { readOnly, className = '', accept = "image/jpeg" } = this.props
    let uP = {
      multiple: false,
      accept: accept,
      showUploadList: false,
      supportServerRender: true,
      customRequest: this.handUpload,
      onPreview: this.handlePreview,
    }
    if (readOnly) {
      uP.disabled = true
    } else {
      uP.onRemove = this.handleRemove
      uP.onChange = this.handleChange
    }

    return (
      <Upload {...uP}>
        {
          <div className={`cover-image ${className}`}>
            {
              coverUrl
                ?
                <ImageItem imageUrl={coverUrl} width={200} aspectRatio='1:1' processType={EmImgProcessType.emGD_HW_L}/>
                :
                <Icon type="plus"/>
            }
          </div>
        }
      </Upload>
    )
  }
}

CoverImage.propTypes = {
  renderType: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
}

export default CoverImage
