// 兼容性处理
import Promise from "promise-polyfill";

const Compatibility = () => {
  if (!window.Promise) {
    window.Promise = Promise;
  }

  window.URL = window.URL || window.webkitURL

  if (typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function (prefix) {
      return this.slice(0, prefix.length) === prefix;
    };
  }

  if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (suffix) {
      return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
  }
}

export default Compatibility
