// detail.js
import { list, detail } from '../../service/orders/data.js'
Page({
  /**
     * 页面的初始数据
     */
  data: {
    isCode:true,
    isPhotoShow:true,
    id: 0,
    error: '',
    photoList: detail.data.photoDetails,
    msgList: [],
    imgList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    let list = [];
    for (let i = 0; i < detail.data.photoDetails.length; i++) {
      list.push(detail.data.photoDetails.procUrl+"");
    }
    that.setData({
      imgList: list,
    });
  },
  /**
   * 验证code是否正确
   */
  formSubmit: function (e) {
    var that = this;
    let id = that.data.id;
    let authCode = e.detail.value.input;
    console.log(authCode);
    if (authCode >= 1000 && authCode <= 9999) {
      this.setData({
        isCode: false
      })
    } else {
      this.setData({
        isCode: true,
      })
    }
  },
  tabClick: function (e) {
    var index = parseInt(e.currentTarget.dataset.index);
    var that = this;
    if (index == 1) {
      that.setData({
        isPhotoShow: true
      });
    } else {
      that.setData({
        isPhotoShow: false
      });
    }
  },
  toView: function (e) {
    var index = parseInt(e.currentTarget.dataset.index);
    var that = this;
    let url = that.data.photoList[index].procUrl;
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: that.data.imgList  // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})