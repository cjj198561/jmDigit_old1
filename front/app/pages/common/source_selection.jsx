import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import debounce from "lodash.debounce";
import netApi from "../../utils/net";
import util from "../../utils/util";
import BaseComponent from "./base.jsx";

import { Button, Select } from "antd";

class SourceSel extends BaseComponent('SourceSel') {
  constructor(props) {
    super(props)
    this.lastFetchId = 0
    this.state = {
      data: [],
      queryParams: {
        pageIndex: 1,
        pageSize: 10,
      },
      count: 0,
      fetching: false,
      keyId: props.keyId
    }
    this.onSearch = debounce(this.onSearch, 800)
  }

  fetchData(p, keyId, isMore = false) {
    // 如果是不可编辑状态，不去请求搜索列表
    if (this.props.disabled) {
      return
    }

    const {
      optionValue = (v) => {
        return {
          value: `${v.id}`,
          text: `${v.id}:${v.name}`,
        }
      }
    } = this.props

    // 基于平台选择,如果没有平台这不做请求处理
    if (p[ keyId ] === undefined || this.state.fetching) {
      return
    }
    // 避免没必要的刷新
    this.lastFetchId += 1
    const fetchId = this.lastFetchId

    this.setState({ fetching: true })
    netApi.get(util.buildQueryUrl(this.props.listUrl, p), (err, json) => {
      if (err) {
        this.setState({ fetching: false })
      } else {
        if (json.success) {
          // for fetch callback order
          if (fetchId !== this.lastFetchId) {
            return;
          }

          if (isMore) {
            let data = _.map(json.data, (v, k) => {
              return optionValue(v)
            })

            this.setState({
              data: [ ...this.state.data, ...data ],
              queryParams: { ...p },
              count: json.count,
              fetching: false,
            })
          } else {
            this.setState({
              data: _.map(json.data, (v, k) => {
                return optionValue(v)
              }),
              queryParams: { ...p },
              count: json.count,
              fetching: false,
            })
          }
        } else {
          this.setState({ fetching: false })
        }
      }
    })
  }

  componentWillMount() {
    this.initData(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.initData(nextProps)
  }

  initData(nextProps) {
    if (nextProps[ nextProps.keyId ] !== this.state.queryParams[ nextProps.keyId ]) {
      this.setState({ data: [] })
      this.fetchData({ ...this.state.queryParams, [nextProps.keyId]: nextProps[ nextProps.keyId ] }, nextProps.keyId)
    } else if (nextProps[ nextProps.keyId ] === undefined) {
      this.setState({ data: [] })
    }
  }

  onSearch = (value) => {
    const { searchKey = 'name' } = this.props
    let p = { ...this.state.queryParams }
    if (p[ searchKey ] !== value) {
      p.pageIndex = 1
    }
    p[ searchKey ] = value

    this.fetchData(p, this.state.keyId)
  }

  onMore = () => {
    if (this.state.fetching) {
      return
    }

    let p = { ...this.state.queryParams }
    p.pageIndex += 1

    this.fetchData(p, this.state.keyId, true)
  }

  onChange = (value) => {
    this.props.onChange(value)
  }

  render() {
    const { mode, showSearch = false, disabled = false, value } = this.props
    const { data = [], count = 0, fetching } = this.state

    let label = (
      <div className="more-box" style={ data.length >= count ? { display: 'none' } : {} }>
        <Button type="primary" loading={fetching} onClick={this.onMore}>加载更多</Button>
      </div>
    )

    return (
      <Select dropdownClassName="custom-select-dropdown-more"
              value={value}
              mode={mode}
              disabled={disabled}
              filterOption={false}
              showSearch={showSearch}
              onSearch={this.onSearch}
              onChange={this.onChange}
              placeholder="请选择">
        <Select.OptGroup label={label}>
          {
            _.map(data, (v, k) => {
              return (
                <Select.Option key={k} value={v.value}>{v.text}</Select.Option>
              )
            })
          }
        </Select.OptGroup>
      </Select>
    )
  }
}

SourceSel.propTypes = {
  keyId: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]),
  showSearch: PropTypes.bool,
  mode: PropTypes.string,
  listUrl: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  optionValue: PropTypes.func,
}

export default SourceSel
