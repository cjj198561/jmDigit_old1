/*
 * Created by cjj on 2017/7/31
 * 信息附加中间件
 * 1.附加客户端信息
 * 2.附加头信息，便于访问后台服务器
 */
export default (app) => {
  app.use(async (ctx, next) => {
    if (!ctx.req.url.startsWith('/static/')) {
      ctx.clientInfo = {
        isMobile: false
      }

      // 判断客户端的浏览器是否是移动端访问
      let userAgent = ctx.req.headers[ 'user-agent' ]
      if (userAgent) {
        let deviceAgent = ctx.req.headers[ 'user-agent' ].toLowerCase()
        let agentID = deviceAgent.match(/(iphone|ipod|ipad|android)/)
        ctx.clientInfo.isMobile = agentID ? true : false
      }

      // 在访问请求里面带上登录用户的信息,用于访问后台服务器的时候传递当前用户信息
      if (ctx.req.url.startsWith('/api/')) {
        ctx.req.headers = {
          ...ctx.req.headers,
          'Login-User': encodeURIComponent(JSON.stringify(ctx.session.user || {}))
        }
      }
    }
    await next()
  })
}
