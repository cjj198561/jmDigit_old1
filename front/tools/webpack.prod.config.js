var getWebPackConfig = require('./webpack.config.js')

module.exports = getWebPackConfig({
  env: 'production'
})
